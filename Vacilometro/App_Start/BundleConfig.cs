﻿using System.Web;
using System.Web.Optimization;

namespace Vacilometro
{
  public class BundleConfig
  {
    // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
    public static void RegisterBundles(BundleCollection bundles)
    {
      bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                  "~/Scripts/jquery-{version}.js"));

      bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                  "~/Scripts/jquery.validate*"));

      // Use the development version of Modernizr to develop with and learn from. Then, when you're
      // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
      bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                  "~/Scripts/modernizr-*"));

      bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js"));

      bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/site.css"));

      bundles.Add(new StyleBundle("~/Content/beguede").Include(
                      "~/Content/beguede.css"
                      ));

      bundles.Add(new ScriptBundle("~/bundles/admin").Include(
                "~/Theme/Admin/js/plugins/plugins.js",
                "~/Theme/Admin/js/appUi-custom.js",
                "~/Theme/Admin/lib/data-tables/jquery.dataTables.min.js", //DataTable
                "~/Theme/Admin/lib/data-tables/dataTables.bootstrap4.min.js", //DataTable
                "~/Theme/Admin/lib/data-tables/dataTables.responsive.min.js", //DataTable
                "~/Theme/Admin/lib/data-tables/responsive.bootstrap4.min.js", //DataTable
                "~/Theme/Admin/js/plugins-custom/datatables-custom.js", //DataTable
                "~/Theme/Admin/lib/bootstrap-notify/bootstrap-notify.min.js", //Notify
                "~/Theme/Admin/js/plugins-custom/bs-notify-custom.js" //Notify
                ));

      bundles.Add(new StyleBundle("~/Content/admin").Include(
                "~/Theme/Admin/lib/bootstrap/dist/css/bootstrap.min.css",
                "~/Theme/Admin/css/plugins/plugins.css",
                "~/Theme/Admin/lib/line-icons/line-icons.css",
                "~/Theme/Admin/lib/font-awesome/css/fontawesome-all.min.css",
                "~/Theme/Admin/lib/data-tables/dataTables.bootstrap4.min.css", //DataTable
                "~/Theme/Admin/lib/data-tables/responsive.bootstrap4.min.css", //DataTable
                "~/Theme/Admin/css/style.css"));

      bundles.Add(new ScriptBundle("~/bundles/classic").Include(
                "~/Theme/Classic/js/plugins/plugins.js",
                "~/Theme/Classic/js/event.custom.js"
                ));

      bundles.Add(new StyleBundle("~/Content/classic").Include(
                "~/Theme/Classic/css/plugins/plugins.css",
                "~/Theme/Classic/css/style-event.css"
                ));

      bundles.Add(new ScriptBundle("~/bundles/timeline").Include(
                "~/Scripts/double-vertical-timeline.js"
                ));

      bundles.Add(new StyleBundle("~/Content/timeline").Include(
                "~/Content/double-vertical-timeline.css"
                ));
    }
  }
}
