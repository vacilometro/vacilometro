﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Vacilometro.Models;

namespace Vacilometro.Controllers
{
    [Authorize(Roles = "Admin")]
    public class EmpresaController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        public ActionResult Index()
        {
            List<Empresa> empresas = _db.Empresas.ToList();
            
            return View(empresas);
        }

        public ActionResult Details(Guid id)
        {
            Empresa empresa = _db.Empresas.Find(id);

            if (empresa == null)
                return HttpNotFound();

            return View(empresa);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Empresa empresa)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    empresa.Id = Guid.NewGuid();
                    _db.Empresas.Add(empresa);
                    _db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return View(empresa);
                }

                return RedirectToAction("Index");
            }

            return View(empresa);
        }

        public ActionResult Edit(Guid id)
        {
            Empresa empresa = _db.Empresas.Find(id);

            if (empresa == null)
                return HttpNotFound();

            return View(empresa);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Empresa empresa)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _db.Entry(empresa).State = EntityState.Modified;
                    _db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return View(empresa);
                }

                return RedirectToAction("Index");
            }
            return View(empresa);
        }

        public ActionResult Delete(Guid id)
        {
            Empresa empresa = _db.Empresas.Find(id);

            if (empresa == null)
                return HttpNotFound();

            return View(empresa);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Empresa empresa = _db.Empresas.Find(id);

            try
            {
                _db.Empresas.Remove(empresa);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                return View(empresa);
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
