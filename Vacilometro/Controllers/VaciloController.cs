﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Vacilometro.Models;

namespace Vacilometro.Controllers
{
  public class VaciloController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        public ActionResult Index()
        {
            List<Vacilo> vacilos = _db.Vacilos.OrderByDescending(x => x.Data).ToList();

            return View(vacilos);
        }

        public ActionResult Details(Guid id)
        {
            Vacilo vacilo = _db.Vacilos.Find(id);

            if (vacilo == null)
                return HttpNotFound();

            return View(vacilo);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.UsuarioId = new SelectList(_db.Usuarios, "Id", "Nome");
            return View();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Vacilo vacilo)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    vacilo.Id = Guid.NewGuid();

                    _db.Vacilos.Add(vacilo);
                    _db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.UsuarioId = new SelectList(_db.Usuarios, "Id", "Nome", vacilo.UsuarioId);

                    return View(vacilo);
                }

                return RedirectToAction("Index");
            }

            ViewBag.UsuarioId = new SelectList(_db.Usuarios, "Id", "Nome", vacilo.UsuarioId);

            return View(vacilo);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Edit(Guid id)
        {
            Vacilo vacilo = _db.Vacilos.Find(id);

            if (vacilo == null)
                return HttpNotFound();

            ViewBag.UsuarioId = new SelectList(_db.Usuarios, "Id", "Nome", vacilo.UsuarioId);

            return View(vacilo);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Vacilo vacilo)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _db.Entry(vacilo).State = EntityState.Modified;
                    _db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.UsuarioId = new SelectList(_db.Usuarios, "Id", "Nome", vacilo.UsuarioId);

                    return View(vacilo);
                }

                return RedirectToAction("Index");
            }

            ViewBag.UsuarioId = new SelectList(_db.Usuarios, "Id", "Nome", vacilo.UsuarioId);

            return View(vacilo);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Delete(Guid id)
        {
            Vacilo vacilo = _db.Vacilos.Find(id);

            if (vacilo == null)
                return HttpNotFound();

            return View(vacilo);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Vacilo vacilo = _db.Vacilos.Find(id);

            try
            {
                _db.Vacilos.Remove(vacilo);
                _db.SaveChanges();

            }
            catch (Exception ex)
            {
                ViewBag.UsuarioId = new SelectList(_db.Usuarios, "Id", "Nome", vacilo.UsuarioId);

                return View(vacilo);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
