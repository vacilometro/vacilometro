﻿using ImageResizer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vacilometro.Models;

namespace Vacilometro.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsuarioController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        public ActionResult Index()
        {
            List<Usuario> usuarios = _db.Usuarios.ToList();

            return View(usuarios);
        }

        public ActionResult Details(Guid id)
        {
            Usuario usuario = _db.Usuarios.Find(id);

            if (usuario == null)
                return HttpNotFound();

            return View(usuario);
        }

        public ActionResult Create()
        {
            ViewBag.EmpresaId = new SelectList(_db.Empresas, "Id", "Nome");
            ViewBag.GeneroId = new SelectList(_db.Generos, "Id", "Nome");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Usuario usuario, HttpPostedFileBase ImageFile)
        {
            usuario.Id = Guid.NewGuid();

            if (ImageFile != null)
            {
                string extension = Path.GetExtension(ImageFile.FileName);

                if (!(extension == ".jpg" || extension == ".png" || extension == ".jpeg"))
                {
                    ModelState.AddModelError("ImageError", $"Formato não aceito. Aceito somente formatos: jpg, jpeg e png");
                }
                else
                {
                    if (ModelState.ContainsKey("ImageError"))
                        ModelState["ImageError"].Errors.Clear();

                    string name = Path.GetFileName($"{usuario.Id.ToString()}.jpg");

                    string path = Path.Combine(Server.MapPath("~/Images/Avatar"), name);
                    ImageFile.SaveAs(path);

                    ResizeSettings resizeSetting = new ResizeSettings
                    {
                        Format = "jpg",
                        Width = 250,
                        Height = 250
                    };

                    ImageBuilder.Current.Build(path, path, resizeSetting);
                }
            }

            if (ModelState.IsValid)
            {
                try
                {
                    
                    _db.Usuarios.Add(usuario);
                    _db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return View(usuario);
                }
                
                return RedirectToAction("Index");
            }

            ViewBag.EmpresaId = new SelectList(_db.Empresas, "Id", "Nome", usuario.EmpresaId);
            ViewBag.GeneroId = new SelectList(_db.Generos, "Id", "Nome", usuario.GeneroId);

            return View(usuario);
        }

        public ActionResult Edit(Guid id)
        {
            Usuario usuario = _db.Usuarios.Find(id);

            if (usuario == null)
                return HttpNotFound();

            ViewBag.EmpresaId = new SelectList(_db.Empresas, "Id", "Nome", usuario.EmpresaId);
            ViewBag.GeneroId = new SelectList(_db.Generos, "Id", "Nome", usuario.GeneroId);

            return View(usuario);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Usuario usuario, HttpPostedFileBase ImageFile)
        {
            if (ImageFile != null)
            {
                string extension = Path.GetExtension(ImageFile.FileName);

                if (!(extension == ".jpg" || extension == ".png" || extension == ".jpeg"))
                {
                    ModelState.AddModelError("ImageError", $"Formato não aceito. Aceito somente formatos: jpg, jpeg e png");
                }
                else
                {
                    if (ModelState.ContainsKey("ImageError"))
                        ModelState["ImageError"].Errors.Clear();

                    string name = Path.GetFileName($"{usuario.Id.ToString()}.jpg");

                    string path = Path.Combine(Server.MapPath("~/Images/Avatar"), name);
                    ImageFile.SaveAs(path);

                    ResizeSettings resizeSetting = new ResizeSettings
                    {
                        Format = "jpg",
                        Width = 250,
                        Height = 250
                    };

                    ImageBuilder.Current.Build(path, path, resizeSetting);
                }
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _db.Entry(usuario).State = EntityState.Modified;
                    _db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.EmpresaId = new SelectList(_db.Empresas, "Id", "Nome", usuario.EmpresaId);
                    ViewBag.GeneroId = new SelectList(_db.Generos, "Id", "Nome", usuario.GeneroId);

                    return View(usuario);
                }

                return RedirectToAction("Index");
            }

            ViewBag.EmpresaId = new SelectList(_db.Empresas, "Id", "Nome", usuario.EmpresaId);
            ViewBag.GeneroId = new SelectList(_db.Generos, "Id", "Nome", usuario.GeneroId);

            return View(usuario);
        }

        public ActionResult Delete(Guid id)
        {
            Usuario usuario = _db.Usuarios.Find(id);

            if (usuario == null)
                return HttpNotFound();

            return View(usuario);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Usuario usuario = _db.Usuarios.Find(id);

            try
            {
                _db.Usuarios.Remove(usuario);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                ViewBag.EmpresaId = new SelectList(_db.Empresas, "Id", "Nome", usuario.EmpresaId);
                ViewBag.GeneroId = new SelectList(_db.Generos, "Id", "Nome", usuario.GeneroId);

                return View(usuario);
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
