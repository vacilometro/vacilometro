﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Vacilometro.Models;

namespace Vacilometro.Controllers
{
  public class HomeController : Controller
  {
    private readonly ApplicationDbContext _db = new ApplicationDbContext();

    public ActionResult Timeline()
    {
      List<Timeline> timeline = _db.Vacilos.OrderByDescending(x => x.Data)
                                   .GroupBy(n => new { n.Data.Value.Year })
                                   .Select(g => new Timeline()
                                   {
                                     Ano = g.Key.Year,
                                     Vacilos = g.OrderByDescending(x => x.Data).ToList()
                                   }).ToList();

      return View(timeline);
    }

    public ActionResult Index()
    {
      return View();
    }

    public ActionResult About()
    {
      ViewBag.Message = "Your application description page.";

      return View();
    }

    public ActionResult Contact()
    {
      ViewBag.Message = "Your contact page.";

      return View();
    }
  }
}