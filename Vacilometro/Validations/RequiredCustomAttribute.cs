﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HomeHit.Validations
{
  public class RequiredCustomAttribute : RequiredAttribute
  {
    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
      if (value == null || Convert.ToString(value) == "")
      {
        return new ValidationResult
            ($"Campo Obrigatório.");
      }
      else
      {
        return ValidationResult.Success;
      }
    }
  }
}