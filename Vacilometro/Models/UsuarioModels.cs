﻿using HomeHit.Validations;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vacilometro.Models
{
    [Table("Usuario")]
    public class Usuario
    {
        [Key]
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        [RequiredCustom]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Display(Name = "User")]
        public string UserId { get; set; }

        [Display(Name = "Empresa")]
        public Guid EmpresaId { get; set; }

        [Display(Name = "Genero")]
        public Guid GeneroId { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Celular")]
        public string Celular { get; set; }

        public virtual ApplicationUser User { get; set; }
        public virtual Empresa Empresa { get; set; }
        public virtual Genero Genero { get; set; }

    }

    [Table("Empresa")]
    public class Empresa
    {
        [Key]
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        [RequiredCustom]
        [Display(Name = "Nome")]
        public string Nome { get; set; }
        
    }

    [Table("Genero")]
    public class Genero
    {
        [Key]
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        [RequiredCustom]
        [Display(Name = "Nome")]
        public string Nome { get; set; }
        
    }

}