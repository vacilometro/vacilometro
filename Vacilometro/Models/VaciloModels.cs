﻿using HomeHit.Validations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vacilometro.Models
{
    [Table("Vacilo")]
    public class Vacilo
    {
        [Key]
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        [RequiredCustom]
        [Display(Name = "Usuário")]
        public Guid UsuarioId { get; set; }

        [RequiredCustom]
        [Display(Name = "Frase")]
        public string Frase { get; set; }

        [Display(Name = "Data")]
        public DateTime? Data { get; set; }

        public virtual Usuario Usuario { get; set; }

    }

    public class Timeline
    {
        public int Ano { get; set; }
        public List<Vacilo> Vacilos { get; set; }
    }

}