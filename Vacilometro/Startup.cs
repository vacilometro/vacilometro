﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Vacilometro.Startup))]
namespace Vacilometro
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
