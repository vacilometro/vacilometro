﻿using System;

namespace Vacilometro.Services
{
    public class GlobalManager
    {

        public static DateTime GetDateTimeNow()
        {
            //return DateTime.Now.AddHours(6);
            return DateTime.Now;
        }

        public static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
                return string.Empty;

            return char.ToUpper(s[0]) + s.Substring(1);
        }

        public static string LowercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
                return string.Empty;

            return char.ToLower(s[0]) + s.Substring(1);
        }

    }
}