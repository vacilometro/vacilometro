﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Vacilometro.Services
{
    public class SendMailerManager
    {
        public static async Task SendEmail(string subject, List<string> emails, string body)
        {
            NetworkCredential credential = new NetworkCredential
            {
                UserName = "username",
                Password = "password"
            };

            try
            {
                string host = "smtp.gmail.com";
                int port = 587;
                bool ssl = true;

                var message = new MailMessage();
                foreach (string email in emails)
                    message.To.Add(new MailAddress(email));

                message.From = new MailAddress("_email_");
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = true;

                using (var smtp = new SmtpClient())
                {
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = ssl;
                    await smtp.SendMailAsync(message);
                }
            }
            catch (Exception ex)
            {

            }
        }

    }
}