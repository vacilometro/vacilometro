﻿using Hangfire.Annotations;
using Hangfire.Dashboard;
using Microsoft.Owin;

namespace Vacilometro.Services
{
  public class HangFireAuthorizationFilter : IDashboardAuthorizationFilter
  {
    public bool Authorize([NotNull] DashboardContext context)
    {

      var owinContext = new OwinContext(context.GetOwinEnvironment());

      return owinContext.Authentication.User.IsInRole("Admin");
    }
  }
}